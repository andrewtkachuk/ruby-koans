# EXTRA CREDIT:
#
# Create a program that will play the Greed Game.
# Rules for the game are in GREED_RULES.TXT.
#
# You already have a DiceSet class and score function you can use.
# Write a player class and a Game class to complete the project.  This
# is a free form assignment, so approach it however you desire.

class Player
  attr_accessor :total
  attr_accessor :number
  @@num = 0
  def initialize
    @total = 0
	@@num += 1
	@number = @@num
  end
end

class Game
  attr_accessor :players
  def initialize
    print "Number of players: "
	@num_players = gets.chomp.to_i
	@players = []
	@num_players.times {@players << Player.new}
	$max_score = 0
  end
end

def want_to_continue
  prng = Random.new
  prng.rand(0..1) == 0 ? false : true
end

def change_values(dice)
  scoring = 0
  hs = Hash.new {0}
  dice.each {|value| hs[value] += 1}
  hs.each do |key, value|
    if key == 1 || key == 5
	  scoring += hs[key]
	end
	if (key == 3 && hs[key] == 3) || (key == 4 && hs[key] == 3) || (key == 2 && hs[key] == 3) || (key == 6 && hs[key] == 3)
	  scoring += 3
	end
  end
  return 5 - scoring == 0 ? 5 : scoring
end

$new_game = Game.new

def total
  $winner = 0
  score = $new_game.players[0].total
  $new_game.players.each do |player|
    score = player.total; $winner = player.number if score < player.total
  end
  return score >= 3000 ? true : false
end

dice = DiceSet.new
while $max_score < 3000
  $new_game.players.each do |player|
    puts "Player #{player.number} turn."
	current = 0
	values = 5
	want = true
	while (values > 0) && want
	  temp_dice = dice.roll(values)
	  values = change_values(temp_dice)
	  temp = score(temp_dice)
	  puts "Player #{player.number} dice #{temp}."
	  if temp == 0
	    current = 0
		break
	  end
	  current += temp
	  want = want_to_continue
	end
	player.total += current
	puts "Player #{player.number} total #{player.total}."
	break if total
  end
end